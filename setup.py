from setuptools import setup, find_packages
from helper import get_version_from_env_variable, get_meta_info_from_version_file

meta = get_meta_info_from_version_file(__file__, "cctools")

setup(
    name="cctools",
    version=get_version_from_env_variable(),
    description=meta.description,
    maintainer=meta.maintainer,
    url=meta.doc_url,
    download_url=meta.dev_url,
    license=meta.license,
    packages=find_packages(),
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'cctools=cctools.cli:cli'
        ]
    }
)
