# --- build-in ---
import tempfile
from pathlib import Path

# --- third-party ---
from click.testing import CliRunner

# --- own ---
from cctools.cli import cli


def test_set_version():
    runner = CliRunner()

    with tempfile.TemporaryDirectory() as tmpdirname:

        file = Path(tmpdirname) / "__version__.py"
        file.touch()

        with open(file, "w") as fp:
            fp.writelines(["__version__ = ''", "\n__license__ = 'FhG'"])

        result = runner.invoke(cli, ["version", "set", str(file), "1.2.3"])
        assert result.exit_code == 0

        with open(file, "r") as fp:
            assert fp.readlines() == ['__version__ = "1.2.3"\n', "__license__ = 'FhG'\n"]
