# --- build-in ---
import os

# --- third-party ---
import pytest

# --- own ---
from cctools import version
from packaging.version import Version


@pytest.fixture()
def uid():
    return abs(hash("feature")) % (10 ** 4)


@pytest.fixture()
def pkg() -> str:
    return "test-pkg"


@pytest.fixture()
def remote(source) -> str:
    if source == "conda":
        return "http://ux1702.iis.fhg.de:8083/fraunhofer/"
    else:
        return "lze-kh"


@pytest.fixture(params=["conda", "conan"])
def source(request) -> str:
    return request.param


@pytest.fixture(params=["develop", "master", "feature", "release-4.5.6"])
def branch(request) -> str:
    return request.param


@pytest.fixture(params=["major", "minor", "patch"])
def increment(request) -> str:
    return request.param


@pytest.fixture()
def stable_version() -> Version:
    return Version("1.2.3")


@pytest.fixture()
def dev_version() -> Version:
    return Version("2.0.1dev4778")


def increment_version(version: Version, kind: str):
    ver = list(version.base_version)
    if kind == "major":
        ver[0] = str(int(ver[0]) + 1)
        ver[2] = "0"
        ver[4] = "0"
    elif kind == "minor":
        ver[2] = str(int(ver[2]) + 1)
        ver[4] = "0"
    elif kind == "patch":
        ver[4] = str(int(ver[4]) + 1)
    return "".join(ver)


def test_auto_versioning(pkg, source, remote, increment, branch, stable_version, dev_version, uid):
    os.environ["CI_COMMIT_REF_NAME"] = branch
    res_ver = version.get_next_version(pkg, remote, source, increment)

    if "master" in branch:
        if source == "conda":
            pytest.skip("Skip because test-pkg=2.0.1.dev4778 is missing in fraunhofer conda channel")
        ref_ver = dev_version.base_version
    elif "develop" in branch:
        ref_ver = increment_version(stable_version, increment)
        ref_ver += ".dev0" if source == "conda" else ""
    elif "release" in branch:
        ref_ver = "4.5.6"
    elif "feature" in branch:
        ref_ver = increment_version(stable_version, increment)
        ref_ver += f"+{uid}" if source == "conan" else f".dev{uid}"
    else:
        raise ValueError

    assert res_ver == Version(ref_ver)


def test_version_init(source, remote):
    os.environ["CI_COMMIT_REF_NAME"] = "develop"
    ref_ver = Version("0.1.0") if source == "conan" else Version("0.1.0.dev0")
    res_ver = version.get_next_version("foo", remote, source, "minor")
    assert res_ver == ref_ver
