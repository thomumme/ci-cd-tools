import typing
import os
import subprocess
from pathlib import Path

PLATFORMS = ["linux-32", "linux-64", "linux-ppc64le", "linux-armv6l", "linux-armv7l",
             "linux-aarch64", "win-32", "win-64", "osx-64", "noarch"]


def call(args) -> str:
    return subprocess.check_output(args).decode("utf-8")


def get_current_branch_name() -> str:
    return os.getenv("CI_COMMIT_REF_NAME", "")


def get_id() -> str:
    name = get_current_branch_name()
    if name:
        return abs(hash(name)) % (10 ** 4)
    else:
        return os.getenv("CI_PIPELINE_ID", None)


def get_conda_packages(conda_bld_path="/opt/conda/conda-bld/") -> typing.List[typing.Tuple[str, Path]]:
    root = Path(conda_bld_path)
    return [(package.parts[-2], package) for package in root.glob("**/*.tar*")
            if package.parts[-2] in PLATFORMS]


def upload_to_conda_channel(server, port, os, package):
    package.rename(package.parent / f"{os}_{package.name}")
    subprocess.call(["curl", "-i", "-F", f"upfile=@{package.parent / f'{os}_{package.name}'}",
                     f"http://{server}:{port}"])


def get_channels() -> str:
    branch = get_current_branch_name()

    if branch == "master":
        channel = "stable"
    else:
        channel = "dev"

    return channel


class Bunch(dict):
    """
    Dictionary-like object that exposes its keys as attributes.
    """

    def __init__(self, **kwargs):
        super().__init__(kwargs)

    def __setattr__(self, key, value):
        self[key] = value

    def __dir__(self):
        return self.keys()

    def __getattr__(self, key):
        try:
            return self[key]
        except KeyError:
            print(f"[WARN] Unknown key '{key}' return empty string!")
            return ""


def get_meta_info_from_version_file(file: str, pkg_location: str) -> Bunch:
    here = Path(file).resolve().parent
    version_py_file = here / pkg_location / '__version__.py'

    if version_py_file.is_file():
        info = {}
        with open(str(version_py_file), 'rb') as f:
            exec(f.read(), info)

        b = Bunch()
        for key, value in info.items():
            b[key.strip("_")] = value

        return b

    else:
        raise ValueError(f"Could not find version file at '{version_py_file}'")


def camel_case_split(val: str) -> list:
    words = [[val[0]]]

    for c in val[1:]:
        if words[-1][-1].islower() and c.isupper():
            words.append(list(c))
        else:
            words[-1].append(c)

    return [''.join(word) for word in words]
