# --- build-in ---
import os
import json
from enum import Enum
from typing import Union, Optional
from tempfile import TemporaryDirectory
from pathlib import Path

# --- third-party ---
import requests
import conda.cli.python_api as conda
from conda.exceptions import PackagesNotFoundError

try:
    from packaging.version import parse, Version
except ImportError:
    from pip._vendor.packaging.version import parse, Version

# --- own ---
from .utils import call, get_current_branch_name, get_id, PLATFORMS


URL_PATTERN = 'https://pypi.python.org/pypi/{package}/json'

# --- enum definitions ---


class ReleaseType(Enum):
    DEV = "dev"
    STABLE = "stable"


class VersionPrimitives(Enum):
    MAJOR = 0
    MINOR = 1
    PATCH = 2


class Sources(Enum):
    CONAN = "get_latest_conan_version"
    CONDA = "get_latest_conda_version"
    PYPI = "get_latest_pypi_version"
    ENV = "get_version_from_env_variable"


# --- helper functions ---

def increment_version(
        version: Version,
        kind: VersionPrimitives = VersionPrimitives.MINOR,
        dev: Union[bool, str] = False,
        target: Sources = Sources.CONDA) -> Version:
    ver_tuple = list(version.release)
    assert len(ver_tuple) <= 3, f"Unknown version type {ver_tuple}"
    assert isinstance(kind, VersionPrimitives), f"Unknown kind of increment '{kind}'. Use [major, minor, patch]"

    if len(ver_tuple) < 3:
        for _ in range(3 - len(ver_tuple)):
            ver_tuple.append(0)

    ver_tuple[kind.value] += 1

    for i in range(kind.value + 1, len(ver_tuple)):
        ver_tuple[i] = 0

    dev_ext = ""

    if not isinstance(dev, bool):
        if target == Sources.CONAN:
            dev_ext = f"+{dev}"
        else:
            dev_ext = f".dev{dev}"

    elif dev is True:
        if target != Sources.CONAN:
            dev_ext = f".dev{0}"

    return parse(".".join([str(n) for n in ver_tuple]) + dev_ext)


# --- functions to retrieve latest version number ---

def get_version_from_env_variable(env_variable="VER_NUM") -> str:
    try:
        return os.environ[env_variable]
    except KeyError as e:
        print("[ERROR] Could not load pkg version from env variable, fall back to 0.0 instead")
        return "0.0.0"


def get_latest_pypi_version(package, url_pattern=URL_PATTERN) -> str:
    """Return version of package on pypi.python.org using json."""
    req = requests.get(url_pattern.format(package=package))
    version = parse('0')
    if req.status_code == requests.codes.ok:
        j = json.loads(req.text.encode(req.encoding or "utf-8"))
        releases = j.get('releases', [])
        for release in releases:
            ver = parse(release)
            if not ver.is_prerelease:
                version = max(version, ver)
    return version


def get_latest_conan_version(package: str, remote: str, channel: ReleaseType) -> Version:

    with TemporaryDirectory() as tmpdirname:
        _ = call(["conan", "search", f"{package}*", "-r", remote, "--json", Path(tmpdirname) / "tmp.json"])

        with open(Path(tmpdirname) / "tmp.json", "r") as f:
            out = json.load(f)

    try:
        versions = []
        for pkg in out["results"][0]["items"]:
            _, ver_str, release_type = pkg["recipe"]["id"].split("/")
            ver_num = ver_str.split("@")[0]

            if release_type == "stable":
                versions.append(parse(ver_num))
            elif "+" in ver_str:
                versions.append(parse(ver_num.replace("+", ".dev")))
            else:
                versions.append(parse(ver_num + ".dev0"))

        if channel == ReleaseType.STABLE:
            versions = [ver for ver in versions if not (ver.is_devrelease or ver.is_prerelease)]

        return sorted(versions)[-1]
    except IndexError:
        return Version("0.0")


def get_latest_conda_version(package: str, channel: str, stage: ReleaseType) -> Version:
    info = list()
    for platform in PLATFORMS:
        try:
            out = conda.run_command(
                conda.Commands.SEARCH, package, "-c", channel, "--json", f"--platform={platform}")[0]
            info += (json.loads(out)[package])
        except (PackagesNotFoundError, KeyError):
            pass

    versions = [parse(pkg["version"]) for pkg in info]

    if stage == ReleaseType.STABLE:
        versions = [ver for ver in versions if not (ver.is_devrelease or ver.is_prerelease)]

    try:
        return sorted(versions)[-1]
    except IndexError:
        return Version("0.0")


def get_next_version(
        package: str,
        remote: str,
        source: str = "conan",
        increment: str = "minor",
        target: Optional[str] = None) -> Version:
    branch = get_current_branch_name()
    target = Sources[source.upper()] if target is None else Sources[target.upper()]
    get_func = eval(Sources[source.upper()].value)
    increment_kind = VersionPrimitives[increment.upper()]

    if "develop" in branch:
        current_version = get_func(package, remote, ReleaseType.STABLE)
        version = increment_version(current_version, kind=increment_kind, dev=True, target=target)
    elif "master" in branch:
        current_version = get_func(package, remote, ReleaseType.DEV)
        version = Version(current_version.base_version)
    elif "release" in branch:
        splits = branch.split("-")
        if len(splits) == 2:
            version = Version(branch.split("-")[-1])
        else:
            print(f"[ERROR] release branch name is misconditioned: {branch}. Fall back to auto versioning!")
            current_version = get_func(package, remote, ReleaseType.STABLE)
            version = increment_version(current_version, kind=increment_kind, dev=True, target=target)
    else:
        current_version = get_func(package, remote, ReleaseType.STABLE)
        version = increment_version(current_version, kind=increment_kind, dev=get_id(), target=target)

    return version

