# --- build-in ---
import tempfile
import fileinput
from pathlib import Path

# --- third-party ---
import click
import docker as docker_api

# --- own ---
from .utils import get_conda_packages, upload_to_conda_channel, call, camel_case_split
from .version import get_next_version, get_latest_pypi_version, get_latest_conda_version, ReleaseType
from .metrics import get_sizes_with_bloaty


@click.group()
def cli():
    pass


@cli.command()
def version():
    return


# --- UPLOAD ---
@cli.group()
def upload():
    pass


@upload.command()
@click.argument("directory", type=click.Path(exists=True), required=True)
@click.argument("server", required=True, type=str)
@click.option("--convert", is_flag=True)
def conda(directory, server: str, convert):
    """
    DIRECTORY is the directory with build conda packages

    SERVER server and port: <name>:<port>
    """
    server, port = server.split(":")

    if convert:
        print("[INFO] Convert pure python package for all platforms")
        for _, package in get_conda_packages(directory):
            call(["conda", "convert", package, "-p", "all", "-o", package.parent])

    print(f"[INFO] Gather conda packages for upload to {server}")
    i = 0
    for os, package in get_conda_packages(directory):
        print(f"[INFO] Upload {package.name} @ {os}")
        i += 1
        upload_to_conda_channel(server, port, os, package)
    if i > 0:
        print(f"[INFO] Finished with uploading {i} packages")
    else:
        raise FileNotFoundError("Could not find any packages to upload")


@upload.command()
@click.argument("directory", type=click.Path(exists=True), required=True)
@click.argument("server", required=True, type=str)
def pip(directory, server):
    """
    DIRECTORY is the directory with build python (sdist) packages

    SERVER server address, e.g. http://ux1702:8082/artifactory/pip/
    """
    for format_ in ("zip", "tar.gz", "whl"):
        for package in Path(directory).glob(f"**/*.{format_}*"):
            print(f"[INFO] Upload '{package.name}' to '{server + package.name}'")
            response = call(["curl", "-T", package, server + package.name])
            print(f"[INFO] Response:\n{response}")

# ---------------


# --- VERSION ---
@cli.group()
def version():
    pass


@version.command()
@click.argument("filename", type=str, required=True)
@click.argument("ver", type=str, required=True)
def set(filename, ver):
    """
    FILENAME of the python version file (e.g. __version__.py)

    VERSION to be set in the version file (__version__ = VERSION)
    """

    assert Path(filename).exists()
    assert Path(filename).suffix == ".py", "Version can only be set on python files."

    with fileinput.FileInput(filename, inplace=True, backup='.bak') as file:
        for line in file:
            if "__version__" in line:
                print(f'__version__ = "{ver}"')
            else:
                print(line)


@version.command()
@click.argument("package", required=True)
def pypi(package):
    """
    PACKAGE Name of the python pypi package to retrieve the latest version number
    """
    print(get_latest_pypi_version(package))


@version.command()
@click.argument("package", required=True)
@click.argument("channel", required=True)
@click.option("--increment", default="minor")
@click.option("--target", default="conda")
def conda(package, channel, increment, target):
    ver = get_next_version(package, channel, "conda", increment, target)
    print(ver)


@version.command()
@click.argument("package", required=True)
@click.argument("remote", required=True)
@click.option("--increment", default="minor")
@click.option("--target", default="conan")
def conan(package, remote, increment, target):
    """
    PACKAGE Name of the conan package to retrieve the next version number

    REMOTE Name of the remote conan repo
    """
    ver = get_next_version(package, remote, "conan", increment, target)
    print(ver)

# ---------------


# ---- BUILD ----
@cli.group()
def supply():
    pass


@supply.command()
@click.argument("directory", type=click.Path(exists=True), required=True)
@click.argument("filename", type=str, required=False)
@click.option("--version", default="latest")
@click.option("--server", default="http://ux1702.iis.fhg.de:8083/fraunhofer/")
@click.option("--registry", default="git01.iis.fhg.de:5005/abt-la/mlv/ci-cd-tools")
def docker(directory, filename, version, server, registry):
    """
    DIRECTORY is the directory with Dockerfiles

    NAME of a specific Dockerfile to be processed (optional)

    VERSION of cctools to be installed in the docker image (no validation!)

    SERVER to be searched for cctools

    REGISTRY for the build docker images
    """

    if version == "latest":
        ver = get_latest_conda_version("cctools", server, ReleaseType.STABLE)
    else:
        ver = version

    client = docker_api.from_env()

    for dockerfile_path in Path(directory).glob('Dockerfile*'):

        if filename is not None and filename not in dockerfile_path.name:
            continue

        with tempfile.NamedTemporaryFile(mode="r+") as tmp_dockerfile:
            with open(str(dockerfile_path), "r") as dockerfile:
                lines = dockerfile.readlines()
            lines.append(f"RUN conda install cctools={ver}\n")
            tmp_dockerfile.writelines(lines)
            tmp_dockerfile.flush()

            name = "-".join([part.lower() for part in camel_case_split(dockerfile.name) if "Dockerfile" not in part])
            full_name = f"{registry}/{name}:{ver}"

            print(f"Build and push '{full_name}'")

            client.images.build(path=str(directory), dockerfile=str(tmp_dockerfile.name),
                                tag=full_name, nocache=True, rm=True, quiet=False)
            client.images.push(repository=full_name)

# ---------------


# ---- METRIC ---

@cli.group()
def metric():
    pass


@metric.command()
@click.argument("binary_file", type=click.Path(exists=True), required=True)
@click.option("--output", type=click.Path(), default="./")
@click.option("--bloaty", type=click.Path(exists=True), default="/bin/bloaty")
def size(binary_file, output, bloaty):
    get_sizes_with_bloaty(Path(binary_file), Path(output), Path(bloaty))
