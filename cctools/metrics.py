# --- build-in ---
from pathlib import Path
from io import StringIO

# --- third-party ---
import pandas as pd

# --- own ---
from .utils import call


def convert_unit(size_in_bytes: int, unit: str) -> float:
    """ Convert the size from bytes to other units like KB, MB or GB"""
    unit = unit.lower()
    if unit == "kb":
        return size_in_bytes / 1024
    elif unit == "mb":
        return size_in_bytes / (1024 * 1024)
    elif unit == "gb":
        return size_in_bytes / (1024 * 1024 * 1024)
    else:
        return size_in_bytes


def get_sizes_with_bloaty(file_path: Path, output_path: Path, bloaty_path: Path):

    if bloaty_path.exists():
        csv_str = call(["bloaty", str(file_path.absolute()), "--csv"])
        df = pd.read_csv(StringIO(csv_str), delimiter=",")
        total_vm_size = convert_unit(sum(df["vmsize"].values), "MB")
        total_file_size = convert_unit(sum(df["filesize"].values), "MB")

        output_path.mkdir(exist_ok=True, parents=True)

        if output_path.name != "metrics.txt":
            output_path /= "metrics.txt"

        metrics = [
            f"TOTAL_VM_SIZE_[MiB] {total_vm_size:.2f}",
            f"TOTAL_FILE_SIZE_[MiB] {total_file_size:.2f}",
        ]

        with open(str(output_path), "w+") as f:
            print(f"[INFO] Write metrics to {output_path.absolute()}")
            for metric in metrics:
                f.write(metric + "\n")
                print(metric)

    else:
        raise RuntimeError("Missing 'bloaty' for generating metrics")
