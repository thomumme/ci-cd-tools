from conans.client.command import main
from pathlib import Path

default_profile = Path("/home/conan/.conan/profiles/default")


def conan(*args):
    try:
        return main(args)
    except SystemExit:
        pass


if __name__ == "__main__":
    if not default_profile.exists():
        print("[INFO] Create new conan default profile")
        conan("profile", "new", "default", "--detect")

    else:
        print("[INFO] conan default profile exists")

    print("[INFO] Set libstdc++11 option in default profile")
    conan("profile", "update", "settings.compiler.libcxx=libstdc++11", "default")
